<?php declare(strict_types=1);

namespace DavidBadura\GitWebhooks\Provider;

use DavidBadura\GitWebhooks\Struct\Commit;
use Symfony\Component\HttpFoundation\Request;

abstract class AbstractProvider
{
	protected function getData(Request $request): array
	{
        $body = $request->getContent();
        return json_decode($body, true);
	}

	/**
	 * @return Commit[]
	 */
	protected function createCommits(array $data)
	{
		$result = [];

		foreach ($data as $row) {
			$result[] = $this->createCommit($row);
		}

		return $result;
	}

	abstract protected function createCommit(array $data): Commit;
}
