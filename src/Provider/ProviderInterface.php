<?php declare(strict_types=1);

namespace DavidBadura\GitWebhooks\Provider;

use DavidBadura\GitWebhooks\Event\AbstractEvent;
use Symfony\Component\HttpFoundation\Request;

interface ProviderInterface
{
	public function create(Request $request): ?AbstractEvent;

	public function support(Request $request): bool;
}
