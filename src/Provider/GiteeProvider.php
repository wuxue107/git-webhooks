<?php

namespace DavidBadura\GitWebhooks\Provider;

use DavidBadura\GitWebhooks\Event\AbstractEvent;
use DavidBadura\GitWebhooks\Event\IssueEvent;
use DavidBadura\GitWebhooks\Event\MergeRequestEvent;
use DavidBadura\GitWebhooks\Event\PingEvent;
use DavidBadura\GitWebhooks\Event\PushEvent;
use DavidBadura\GitWebhooks\Struct\Commit;
use DavidBadura\GitWebhooks\Struct\Repository;
use DavidBadura\GitWebhooks\Struct\User;
use DavidBadura\GitWebhooks\Util;
use Symfony\Component\HttpFoundation\Request;

class GiteeProvider extends AbstractProvider implements ProviderInterface
{
    public const NAME = 'gitee';

    /**
     * @param Request $request
     * @return AbstractEvent
     * @throws \Exception
     */
    public function create(Request $request): AbstractEvent
    {
        $data = $this->getData($request);
        if (!$data) {
            return null;
        }

        if("true" === $request->headers->get('HTTP_X_GITEE_PING')){
            return $this->createPingEvent($data);
        }

        switch ($request->headers->get('X-Gitee-Event')) {
            case 'Push Hook':
            case 'Tag Push Hook':
                return $this->createPushEvent($data);
            case 'Issue Hook':
                return $this->createIssueEvent($data);
            case 'Merge Request Hook':
                return $this->createMergeRequestEvent($data);
            case 'Note Hook':
            default:
                return null;
        }
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function support(Request $request): bool
    {
        return $request->headers->has('X-Gitee-Event');
    }


    /**
     * @param $data
     * @return PushEvent
     * @throws \Exception
     */
    private function createPushEvent($data)
    {
        $event = new PushEvent();
        $event->provider = self::NAME;
        $event->before = $data['before'];
        $event->after = $data['after'];
        $event->ref = $data['ref'];


        $event->user = $this->createUser($data['sender']);
        $event->repository = $this->createRepository($data['repository']);
        $event->commits = $this->createCommits($data['commits']);

        if (!$event->commits and $data['head_commit']) {
            $event->commits[] = $this->createCommit($data['head_commit']);
        }

        $event->type = Util::getPushType($event->ref);

        if ($event->type == PushEvent::TYPE_BRANCH) {
            $event->branchName = Util::getBranchName($event->ref);
        }
        else {
            $event->tagName = Util::getTagName($event->ref);
        }

        return $event;
    }

    /**
     * @param array $data
     * @return MergeRequestEvent
     * @throws \Exception
     */
    private function createMergeRequestEvent(array $data)
    {
        $event = new MergeRequestEvent();

        $event->provider = self::NAME;
        $event->id = $data['pull_request']['id'];
        $event->title = $data['pull_request']['title'];
        $event->description = $data['pull_request']['body'];

        $event->targetBranch = $data['pull_request']['base']['ref'];
        $event->sourceBranch = $data['pull_request']['head']['ref'];
        $event->state = $this->pullRequestState($data['pull_request']);
        $event->createdAt = new \DateTime($data['pull_request']['created_at']);
        $event->updatedAt = new \DateTime($data['pull_request']['updated_at']);

        $event->user = $this->createUser($data['pull_request']['user']);
        $event->repository = $this->createRepository($data['pull_request']['base']['repo']);
        $event->sourceRepository = $this->createRepository($data['pull_request']['head']['repo']);

        // TODO request data from $data['pull_request']['commits_url']
        $event->lastCommit = new Commit();
        $event->lastCommit->id = $data['pull_request']['head']['sha'];

        return $event;
    }

    public function createUser($userData){
        $user = new User();
        $user->id = $userData['id'];
        $user->name = $userData['username']??$userData['name']??$userData['login'];
        $user->email = $userData['email']??null;
        return $user;
    }
    /**
     * @param array $data
     * @return Repository
     */
    private function createRepository(array $data)
    {
        $repository = new Repository();

        $repository->id = $data['id'];
        $repository->name = $data['name'];
        $repository->description = $data['description'];
        $repository->namespace = $this->extractNamespace($data['full_name']);
        $repository->url = $data['ssh_url'];
        $repository->homepage = $data['html_url'];

        return $repository;
    }

    /**
     * @param array $data
     * @return Commit
     * @throws \Exception
     */
    protected function createCommit(array $data): Commit
    {
        $commit = new Commit();

        $commit->id = $data['id'];
        $commit->message = $data['message'];
        $commit->date = new \DateTime($data['timestamp']);

        $commit->author = $this->createUser($data['author']);

        return $commit;
    }

    /**
     * @param string $fullName
     * @return string
     */
    private function extractNamespace($fullName)
    {
        return dirname($fullName);
    }

    /**
     * @param array $pullRequest
     * @return string
     */
    private function pullRequestState(array $pullRequest)
    {
        if ($pullRequest['state'] === 'open') {
            return MergeRequestEvent::STATE_OPEN;
        }

        if ($pullRequest['merged_at']) {
            return MergeRequestEvent::STATE_MERGED;
        }

        return MergeRequestEvent::STATE_CLOSED;
    }

    private function buildEventBase(AbstractEvent $event,array $data){
        $event->user = $this->createUser($data['sender']);
        $event->repository = $this->createRepository($data['repository']);
        $event->provider = self::NAME;
    }

    private function createIssueEvent(array $data)
    {
        $event = new IssueEvent();
        $this->buildEventBase($event,$data);
        $event->action = $data['action']??'none';
        $event->title = $data['title'];
        $event->description = $data['description'];

        return $event;
    }

    private function createPingEvent(array $data)
    {
        $event = new PingEvent();
        $this->buildEventBase($event,$data);
        $event->action = $data['action']??'none';
        $event->title = $data["hook_name"] . '-' . $data['hook_id'];
        $event->description = '';

        return $event;
    }
}
